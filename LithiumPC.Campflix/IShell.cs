﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IShell.cs" company="Lithium PC, LLC">
//   Copyright (C) 2013 Lithium PC, LLC
//   Released to the public domain, see COPYING for details
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace LithiumPC.Campflix
{
    /// <summary>
    ///     The shell interface
    /// </summary>
    internal interface IShell
    {
    }
}