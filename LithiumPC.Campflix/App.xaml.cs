﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="Lithium PC, LLC">
//   Copyright (C) 2013 Lithium PC, LLC
//   Released to the public domain, see COPYING for details
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace LithiumPC.Campflix
{
    using System.Windows;

    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}