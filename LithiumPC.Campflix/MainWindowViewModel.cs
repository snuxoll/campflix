﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindowViewModel.cs" company="Lithium PC, LLC">
//   Copyright (C) 2013 Lithium PC, LLC
//   Released to the public domain, see COPYING for details
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace LithiumPC.Campflix
{
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Threading.Tasks;

    using Caliburn.Micro;

    using LithiumPC.Campflix.Netflix;

    /// <summary>
    ///     ViewModel for the application shell.
    /// </summary>
    [Export(typeof(IShell))]
    internal class MainWindowViewModel : Screen, IShell, IHandle<List<Title>>
    {
        #region Fields

        /// <summary>
        ///     Backing field for the SearchQuery property.
        /// </summary>
        private string searchQuery = string.Empty;

        /// <summary>
        ///     Backing field for the Titles property.
        /// </summary>
        private BindableCollection<Title> titles;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainWindowViewModel" /> class.
        /// </summary>
        public MainWindowViewModel()
        {
            this.Titles = new BindableCollection<Title>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the search query.
        /// </summary>
        public string SearchQuery
        {
            get
            {
                return this.searchQuery;
            }

            set
            {
                if (value != this.searchQuery)
                {
                    this.searchQuery = value;
                    this.NotifyOfPropertyChange(() => this.SearchQuery);
                }
            }
        }

        /// <summary>
        ///     Gets the list of titles.
        /// </summary>
        public BindableCollection<Title> Titles
        {
            get
            {
                return this.titles;
            }

            private set
            {
                if (value != this.titles)
                {
                    this.titles = value;
                    this.NotifyOfPropertyChange(() => this.Titles);
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the event aggregator.
        /// </summary>
        [Import]
        private IEventAggregator EventAggregator { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Update the titles list with the search results.
        /// </summary>
        /// <param name="results">
        /// The search results containing titles to display.
        /// </param>
        public void Handle(List<Title> results)
        {
            this.Titles.Clear();
            this.Titles.AddRange(results);
        }

        /// <summary>
        ///     Search the catalog.
        /// </summary>
        public void Search()
        {
            Task.Factory.StartNew(
                (query) =>
                    {
                        var catalog = IoC.Get<NetflixCatalog>();
                        IQueryable<Title> results = from title in catalog.Titles
                                                    where title.Name.Contains((string)query)
                                                    select title;
                        IoC.Get<IEventAggregator>().Publish(results.ToList());
                    }, 
                this.SearchQuery);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Initialize the ViewModel.
        /// </summary>
        protected override void OnInitialize()
        {
            this.DisplayName = "Campflix";
            this.EventAggregator.Subscribe(this);

            base.OnInitialize();
        }

        #endregion
    }
}