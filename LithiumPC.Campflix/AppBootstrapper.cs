﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppBootstrapper.cs" company="Lithium PC, LLC">
//   Copyright (C) 2013 Lithium PC, LLC
//   Released to the public domain, see COPYING for details
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace LithiumPC.Campflix
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.ComponentModel.Composition.Primitives;
    using System.Linq;

    using Caliburn.Micro;

    using LithiumPC.Campflix.Netflix;

    /// <summary>
    ///     Application bootstrapper, responsible for configuration of the IoC
    ///     and MVVM frameworks.
    /// </summary>
    internal class AppBootstrapper : Bootstrapper<IShell>
    {
        #region Fields

        /// <summary>
        ///     Composition container for the MEF framework.
        /// </summary>
        private CompositionContainer container;

        #endregion

        #region Methods

        /// <summary>
        /// Satisfies imports for an object.
        /// </summary>
        /// <param name="instance">
        /// Object to satisfy imports for.
        /// </param>
        protected override void BuildUp(object instance)
        {
            this.container.SatisfyImportsOnce(instance);
        }

        /// <summary>
        ///     Configure the MEF framework and our applications catalog
        /// </summary>
        protected override void Configure()
        {
            this.container =
                new CompositionContainer(
                    new AggregateCatalog(
                        AssemblySource.Instance.Select(x => new AssemblyCatalog(x)).OfType<ComposablePartCatalog>()));

            var batch = new CompositionBatch();

            batch.AddExportedValue<IWindowManager>(new WindowManager());
            batch.AddExportedValue<IEventAggregator>(new EventAggregator());
            batch.AddExportedValue(new NetflixCatalog(new Uri("http://odata.netflix.com/Catalog/")));
            batch.AddExportedValue(this.container);

            this.container.Compose(batch);
        }

        /// <summary>
        /// Gets all instances for a given service contract.
        /// </summary>
        /// <param name="serviceType">
        /// Type name of the service contract.
        /// </param>
        /// <returns>
        /// An <see cref="IEnumerable"/> containing all discovered instances.
        /// </returns>
        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return this.container.GetExportedValues<object>(AttributedModelServices.GetContractName(serviceType));
        }

        /// <summary>
        /// Get an instance for a given service contract.
        /// </summary>
        /// <param name="serviceType">
        /// Type name of the service contract.
        /// </param>
        /// <param name="key">
        /// String format of the service, this will be used over the type name if it not nulled.
        /// </param>
        /// <returns>
        /// The first <see cref="object"/> that satisfies the contract.
        /// </returns>
        protected override object GetInstance(Type serviceType, string key)
        {
            string contract = string.IsNullOrEmpty(key) ? AttributedModelServices.GetContractName(serviceType) : key;
            IEnumerable<object> exports = this.container.GetExportedValues<object>(contract);

            if (exports.Count() > 0)
            {
                return exports.First();
            }

            throw new Exception(string.Format("Could not locate any instances of contract {0}.", contract));
        }

        #endregion
    }
}